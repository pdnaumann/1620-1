
public class Cat extends AAnimal{

	@Override
	public void feed(IFood food) {
		if(food instanceof CatFood)
		{
			improveHappiness();
		}
	}

}
